#
# https://s3.amazonaws.com/ocean-templates/vpc/vpc-networking.yaml
#
# Sets up the foundational infrastructure.
#
# Maintenance: Keep the Mappings section up to date.
#
# Allocated subnets by the vpc.yaml template, common to all environments:
#   x.x.0.0/24     VPC common public subnet 1, for Bastions and load balancers
#   x.x.1.0/24     VPC common public subnet 2, for Bastions and load balancers
#   x.x.2.0/24     VPC common public subnet 3, for Bastions and load balancers
#   x.x.3.0/24     VPC common private subnet 1, for Public Apps such as the admin web app
#   x.x.4.0/24     VPC common private subnet 2, for Public Apps such as the admin web app
#   x.x.5.0/24     VPC common private subnet 3, for Public Apps such as the admin web app
#   x.x.6.0/24     (not allocated)
#   x.x.7.0/24     (not allocated)
#

AWSTemplateFormatVersion: "2010-09-09"

Description: "VPC networking: IGW, NATs, common subnets, etc"


#-------------------------------------------------------------------------
Parameters:

  ExportPrefix:
    Description: The prefix for the exported values
    Type: String

  VPCFirstTwoOctets:
    Description: "Must be a private IP range such as 192.168, 172.16 to 172.31, or 10.0 to 10.255. The VPC CIDR range will be x.x.0.0/16."
    Type: String
    Default: "10.0"
    AllowedPattern: "^(192\\.168)|(172\\.(16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31))|(10\\.[0-9]{1,3})$"
    ConstraintDescription: Must be a private IP range such as 192.168, 172.16 to 172.31, or 10.0 to 10.255

  AdminRange:
    Description: CIDR block from which admin access to instances will be permitted (web and SSH), or blank
    Type: String
    Default: ""


#-------------------------------------------------------------------------
Conditions:

  HasAdminRange: !Not [!Equals [!Ref AdminRange, ""]]
  NoAdminRange: !Not [Condition: HasAdminRange]


#-------------------------------------------------------------------------
Resources:

  ############################################################
  #
  # The VPC itself.
  #
  # Everything we set up is put inside this VPC.
  #
  ############################################################

  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".0.0/16"]]
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} VPC"


  ############################################################
  #
  # The Internet Gateway.
  #
  # The IGW allows _public_ resources within our VPC to access
  # the internet, and vice versa. If the routing table for
  # a VPC subnet routes to the IGW as default, then the subnet
  # is a public subnet: it can be reached by the internet,
  # and it can reach the internet.
  #
  ############################################################

  InternetGateway:
    Type: "AWS::EC2::InternetGateway"
    Properties:
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} IGW"

  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway


  ############################################################
  #
  # NAT, NAT2, NAT3
  #
  # The NATs allow resources in _private_ subnets in the VPC
  # to reach the internet (via the IGW). It's a one-way
  # connection: the internet cannot reach resources via NAT.
  # We use three NATs per VPC, one in each AZ; in this way,
  # all egress traffic comes from the same three IPs, which
  # we need for whitelisting ourselves with external service
  # partners.
  #
  ############################################################

  NAT1:
    DependsOn: InternetRoute
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt EIP1.AllocationId
      SubnetId: !Ref CommonPublicSubnet1

  NAT2:
    DependsOn: InternetRoute
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt EIP2.AllocationId
      SubnetId: !Ref CommonPublicSubnet2

  NAT3:
    DependsOn: InternetRoute
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt EIP3.AllocationId
      SubnetId: !Ref CommonPublicSubnet3


  EIP1:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc

  EIP2:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc

  EIP3:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc



  ############################################################
  #
  # Public subnets for Bastion servers, NAT, etc.
  #
  # Note that as public subnets, there is a default route to
  # the IGW in the routing table.
  #
  ############################################################

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} public routing table"


  InternetRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: "0.0.0.0/0"
      GatewayId: !Ref InternetGateway


  CommonPublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [0, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".0.0/24"]]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common public subnet 1"

  CommonPublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [1, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".1.0/24"]]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common public subnet 2"

  CommonPublicSubnet3:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [2, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".2.0/24"]]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common public subnet 3"


  CommonPublicSubnet1Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CommonPublicSubnet1

  CommonPublicSubnet2Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CommonPublicSubnet2

  CommonPublicSubnet3Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CommonPublicSubnet3


  ############################################################
  #
  # Private subnets. For things which must access the web from
  # our NAT addresses for whitelisting purposes.
  #
  # Note that as private subnets, there is a default route to
  # the NAT for the AZ in the routing tables (three in total).
  #
  ############################################################

  PrivateRouteTable1:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} AZ1 private routing table"

  NATRoute1:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable1
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId: !Ref NAT1


  PrivateRouteTable2:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} AZ2 private routing table"

  NATRoute2:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable2
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId: !Ref NAT2


  PrivateRouteTable3:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} AZ3 private routing table"

  NATRoute3:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable3
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId: !Ref NAT3


  CommonPrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [0, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".3.0/24"]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common private subnet AZ1"

  CommonPrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [1, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".4.0/24"]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common private subnet AZ2"

  CommonPrivateSubnet3:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [2, !GetAZs ""]
      CidrBlock: !Join ["", [!Ref VPCFirstTwoOctets, ".5.0/24"]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} common private subnet AZ3"


  CommonPrivateSubnet1Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PrivateRouteTable1
      SubnetId: !Ref CommonPrivateSubnet1

  CommonPrivateSubnet2Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PrivateRouteTable2
      SubnetId: !Ref CommonPrivateSubnet2

  CommonPrivateSubnet3Assoc:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref PrivateRouteTable3
      SubnetId: !Ref CommonPrivateSubnet3


  ############################################################
  #
  # S3 and DynamoDB endpoints
  #
  ############################################################

  DynamoDBEndpoint:
    Type: "AWS::EC2::VPCEndpoint"
    Properties:
      RouteTableIds:
        - !Ref PublicRouteTable
        - !Ref PrivateRouteTable1
        - !Ref PrivateRouteTable2
        - !Ref PrivateRouteTable3
      ServiceName:
        !Sub "com.amazonaws.${AWS::Region}.dynamodb"
      VpcId: !Ref VPC

  S3Endpoint:
    Type: "AWS::EC2::VPCEndpoint"
    Properties:
      RouteTableIds:
        - !Ref PublicRouteTable
        - !Ref PrivateRouteTable1
        - !Ref PrivateRouteTable2
        - !Ref PrivateRouteTable3
      ServiceName:
        !Sub "com.amazonaws.${AWS::Region}.s3"
      VpcId: !Ref VPC


  ############################################################
  #
  # VPC Flow Log
  #
  ############################################################

  VPCFlowLog:
    Type: AWS::EC2::FlowLog
    Properties:
      DeliverLogsPermissionArn : !GetAtt VPCFlowLogRole.Arn
      LogGroupName : !Sub "${ExportPrefix}FlowLog"
      ResourceId : !Ref VPC
      ResourceType : VPC
      TrafficType : ALL

  VPCFlowLogRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: "vpc-flow-logs.amazonaws.com"
            Action: ["sts:AssumeRole"]
      Path: /

  VPCFlowLogRolePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: "VPCFlowLogRolePolicy"
      PolicyDocument:
        Statement:
        - Action:
          - logs:CreateLogGroup
          - logs:CreateLogStream
          - logs:PutLogEvents
          - logs:DescribeLogGroups
          - logs:DescribeLogStreams
          - ec2:*FlowLogs
          Resource: "*"
          Effect: Allow
      Roles:
        - !Ref VPCFlowLogRole


  ############################################################
  #
  # A Bastion SG from which everything should accept
  # traffic. This SG is global, i.e. it can reach across all
  # environments.
  #
  ############################################################

  BastionSG:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: For Bastion servers. Admin SSH and HTTP in, all internal IPs out.
      VpcId: !Ref VPC
      Tags:
        - Key: "Name"
          Value: "Bastion Server Security Group"
      SecurityGroupIngress:
        - Fn::If:
          - NoAdminRange
          - !Ref AWS::NoValue
          - IpProtocol: tcp
            FromPort: 22
            ToPort: 22
            CidrIp: !Ref AdminRange
        - Fn::If:
          - NoAdminRange
          - !Ref AWS::NoValue
          - IpProtocol: tcp
            FromPort: 80
            ToPort: 80
            CidrIp: !Ref AdminRange
      SecurityGroupEgress:
        - Fn::If:
          - NoAdminRange
          - !Ref AWS::NoValue
          - IpProtocol: '-1'
            CidrIp: 0.0.0.0/0


  ############################################################
  #
  # Global RDS subnet group for the VPC private subnets
  #
  ############################################################

  RDSSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: !Sub "${ExportPrefix} Global Subnet Group for RDS DB instances in the VPC private subnets"
      SubnetIds:
        - !Ref CommonPrivateSubnet1
        - !Ref CommonPrivateSubnet2
        - !Ref CommonPrivateSubnet3
      Tags:
        - Key: "Name"
          Value: !Sub "${ExportPrefix} Global RDS DB Subnet Group"


#-------------------------------------------------------------------------
Outputs:

  VPC:
    Description: The ID of the VPC
    Value: !Ref VPC
    Export:
      Name:
        !Sub "${ExportPrefix}-VPC"

  VPCCidrBlock:
    Description: The VPC CIDR range
    Value: !Join ["", [!Ref VPCFirstTwoOctets, ".0.0/16"]]
    Export:
      Name:
        !Sub "${ExportPrefix}-VPCCidrBlock"

  InternetGateway:
    Description: The Internet Gateway the VPC uses
    Value: !Ref InternetGateway
    Export:
      Name:
        !Sub "${ExportPrefix}-InternetGateway"

  CommonPublicSubnet1:
    Description: Public subnet 1 common to the whole VPC (for Bastion servers, NAT, ELB, etc)
    Value: !Ref CommonPublicSubnet1
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPublicSubnet1"   # Used only by Bastion

  CommonPublicSubnet2:
    Description: Public subnet 2 common to the whole VPC (for ELB, etc)
    Value: !Ref CommonPublicSubnet2
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPublicSubnet2"   # Currently unused

  CommonPublicSubnet3:
    Description: Public subnet 3 common to the whole VPC (for ELB, etc)
    Value: !Ref CommonPublicSubnet3
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPublicSubnet3"   # Currently unused

  CommonPrivateSubnet1:
    Description: Private subnet 1 common to the whole VPC (for Bastion servers, NAT, ELB, etc)
    Value: !Ref CommonPrivateSubnet1
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPrivateSubnet1"

  CommonPrivateSubnet2:
    Description: Private subnet 2 common to the whole VPC (for ELB, etc)
    Value: !Ref CommonPrivateSubnet2
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPrivateSubnet2"

  CommonPrivateSubnet3:
    Description: Private subnet 3 common to the whole VPC (for ELB, etc)
    Value: !Ref CommonPrivateSubnet3
    Export:
      Name:
        !Sub "${ExportPrefix}-CommonPrivateSubnet3"


  NAT1:
    Description: NAT 1, residing in common public subnet 1
    Value: !Ref NAT1
    Export:
      Name:
        !Sub "${ExportPrefix}-NAT1"

  NAT2:
    Description: NAT, residing in common public subnet 2
    Value: !Ref NAT2
    Export:
      Name:
        !Sub "${ExportPrefix}-NAT2"

  NAT3:
    Description: NAT, residing in common public subnet 3
    Value: !Ref NAT3
    Export:
      Name:
        !Sub "${ExportPrefix}-NAT3"


  VPCFlowLogsUrl:
    Description: The VPC Flow Log in Cloudwatch
    Value: !Sub "https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=${AWS::Region}#logStream:group=${ExportPrefix}FlowLog;streamFilter=typeLogStreamPrefix"


  BastionSG:
    Description: A security group that allows an Bastion host to connect to all instances in the subnet
    Value: !Ref BastionSG
    Export:
      Name:
        !Sub "${ExportPrefix}-BastionSG"


  RDSSubnetGroup:
    Description: A Subnet Group for RDS DBs placed in the VPC private subnets
    Value: !Ref RDSSubnetGroup
    Export:
      Name:
        !Sub "${ExportPrefix}-RDSSubnetGroup"
