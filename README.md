# cfn-templates

**NB: This repository is undergoing radical change to support ECS Fargate.
    It won't work at the moment. Meanwhile, use the local Docker Compose
    setup as described in the repos.**

This repository contains AWS CloudFormation templates to set up and manage the
whole Ocean system on AWS.

The template structure is a layered one. Different aspects of the installation
have their own CloudFormation templates, in order to allow easy delegation of
responsibilities.

Application CloudFormation template maintenance is designed to be delegated to
developer groups responsible for each application or service. Dev and prod
may differ in things like instance sizes and redundancy (such things as failover
and multi Availability Zone RDS, for instance, are only required in prod).
Otherwise, environments should be logically uniform.


## Basic Templates

There are templates for basic networking and VPC setup. There are also templates
to generate passwords for DBs and users, and templates that abstract away the
need to manually define IP ranges for VPC subnets (so multiple environments can
be deployed and torn down without human intervention, e.g. by the CI system).

At the moment you can install in the following AWS regions:

* `us-east-1`, North Virginia
* `us-east-2`, Ohio
* `us-west-2`, Oregon
* `eu-west-1`, Ireland

We're using the various AWS DevOps offerings (CodePipeline, CodeCommit, CodeBuild,
CodeDeploy). The above four regions are the only AWS regions which support all
four above DevOps tools.

If you modify the pipeline sources after creation (using the AWS console) to
fetch from GitHub/GitLab/BitBucket rather than from CodeCommit the following
three regions also become possible:

* `eu-central-1`, Frankfurt
* `ap-southeast-1`, Singapore
* `ap-southeast-2`, Sydney


## Installation

### Preparations

* If you're just testing out Ocean, you can simply point to the open source bucket
  when prompted for where the source CloudFormation templates are located. If so,
  use the bucket name `ocean-templates`.

  For doing real work with Ocean, you probably will have to tailor the
  CloudFormation templates at some point. Create an S3 bucket and copy the
  contents of the public S3 bucket `ocean-templates` into it. Your new bucket
  can be called anything, e.g. `projectname-cloudformation-templates`.
  Substitute your own bucket name as appropriate in the URLs below.

* Clone the Ocean source repos from GitHub to CodeCommit. Keep the names exactly
  as they are.


### Helper Stacks

The auto subnet management facility lets you allocate subnets within a VPC
without ever having to bother about collisions. We use a Lambda function and a
very small DynamoDB table.

* Create the `https://s3.amazonaws.com/ocean-templates/autosubnet/autosubnet.yaml`
stack. Name it `AutoSubnet`.

The templates can create all DB and user passwords for you. To install the
Lambda function that allows this:

* Create the `https://s3.amazonaws.com/ocean-templates/random.yaml` stack. Name
  it `Random`.


### Domain

* Set up a domain on AWS Route 53. Request a SSL certificate for your domain
  using AWS Certificate Manager. Take note of the ARN.

  You can use an existing domain. The top level will be untouched; only a few
  subdomains such as `dev-api.` will be created, and an internal DNS domain
  set up.


### VPC

* Create the `https://s3.amazonaws.com/ocean-templates/vpc.yaml` stack.  Name it
  `VPC`. This will create the Ocean VPC and export vital common data used by
  other CloudFormation templates.


### Dev and prod

* Create the `https://s3.amazonaws.com/ocean-templates/env.yaml` stack. Name it
  `Dev` and set the environment name to `dev`. This will set up the dev
   environment inside the VPC.

* To deploy the production environment, create the `https://s3.amazonaws.com/ocean-templates/env.yaml`
  stack again, but this time name it `Prod`, set its name to `prod`, and set
  `Prodlike environment` to true.
  The prod environment will be created in the VPC. You will notice that it has
  failover and high-availability features for servers and databases.

* Any other environment is created in the same way. Make sure the environment
  name is DNS compliant.

### Done

<img src="etc/vpc_and_dev_env.png" align="right" width="400px">

Your Ocean system is ready and running. The API can be reached at
  * https://api.example.com (Prod)
  * https://dev-api.example.com (Dev)
  * https://staging-api.example.com (Staging)

Explore the pipelines set up by Ocean, check out the log aggregation, the
alarms, run the Ocean Tool... Check out the programming paradigm and make a few
CloudFormation stack updates to change your Ocean system dynamically.


### Bastion Server

<img src="etc/ocean_pipeline.png" align="right" width="200px">

To SSH into or make HTTP requests to instances in private subnets you need a
Bastion server. You can create one when needed using the
`https://s3.amazonaws.com/ocean-templates/bastion.yaml` template. It can be
deleted when not in use.


### Stack naming

You can of course name the stacks anything you like. The above names will allow
you to use the defaults provided in the template forms, which is what you want
if you install a single Ocean system.

A single Ocean system may have any number of environments within its VPC.
Environments can be created and deleted dynamically, e.g. for automated testing
without any human input. Each Ocean system has room for many environments, each
with full network separation. There is no need to enter subnet ranges anywhere,
as the AutoSubnet custom resource handles this automatically.


### Pipelines

You get an AWS CodePipeline for each service.

Each pipeline uses CodeBuild for testing and artefact builds.

Deployment uses AWS CodeDeploy, which means you can switch between rolling
in-place deployments and Blue/Green deployment for an environment simply by
running a CloudFormation update.


### Multiple Ocean systems

You can also create multiple Ocean systems in the same account. If you do, you
will need a separate domain for each full Ocean system. Redo all above steps but
skip the Auto Subnet and Random sections.
