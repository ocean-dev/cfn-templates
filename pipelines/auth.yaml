
# https://s3-eu-west-1.amazonaws.com/ocean-templates/auth.yaml
#
# First builds the service in each existing environment.
#
# Then builds one CodePipeline for the Service, with stages corresponding
# to the environments. If no staging environment is present,
# deployment will be continuous. If there is a staging environment,
# continuous delivery with breaks between deployment stages will be
# created.
#

AWSTemplateFormatVersion: "2010-09-09"

Description: "[INTERNAL] The Auth service pipeline"


#-------------------------------------------------------------------------
Parameters:

  ServiceName:
    Description: The name of the service
    Type: String

  OceanStack:
    Description: The Ocean stack for which this is done
    Type: String
    MinLength: 1
    MaxLength: 255
    AllowedPattern: "^[a-zA-Z][-a-zA-Z0-9]*$"
    Default: Ocean

  NumberOfAZs:
    Type: Number
    Default: 3
    AllowedValues: [2, 3]

  DevPublicDomainName:
    Type: String
  DevSizeOfEnvironment:
    Type: String
    AllowedValues: [Minimal, Small, Medium, Large]
  DevTypeOfDeployment:
    Type: String
    AllowedValues: ["Rolling In Place", "Blue/Green"]
  DevALBListener:
    Type: String
  DevPrivateSubnetA:
    Type: String
  DevPrivateSubnetB:
    Type: String
  DevPrivateSubnetC:
    Type: String
  DevMicroserviceSG:
    Type: String

  StagingPublicDomainName:
    Type: String
  StagingSizeOfEnvironment:
    Type: String
    AllowedValues: [None, Minimal, Small, Medium, Large]
  StagingTypeOfDeployment:
    Type: String
    AllowedValues: ["Rolling In Place", "Blue/Green"]
  StagingALBListener:
    Type: String
  StagingPrivateSubnetA:
    Type: String
  StagingPrivateSubnetB:
    Type: String
  StagingPrivateSubnetC:
    Type: String
  StagingMicroserviceSG:
    Type: String

  ProdPublicDomainName:
    Type: String
  ProdSizeOfEnvironment:
    Type: String
    AllowedValues: [None, Minimal, Small, Medium, Large]
  ProdTypeOfDeployment:
    Type: String
    AllowedValues: ["Rolling In Place", "Blue/Green"]
  ProdALBListener:
    Type: String
  ProdPrivateSubnetA:
    Type: String
  ProdPrivateSubnetB:
    Type: String
  ProdPrivateSubnetC:
    Type: String
  ProdMicroserviceSG:
    Type: String


#-------------------------------------------------------------------------
Conditions:

  CreateProdResources:
    !Not [!Equals [!Ref ProdSizeOfEnvironment, None]]
  CreateStagingResources:
    !Not [!Equals [!Ref StagingSizeOfEnvironment, None]]
  CreateDevResources:
    !Not [!Equals [!Ref DevSizeOfEnvironment, None]]

  OnlyDev:
    !And [!Not [Condition: CreateStagingResources], !Not [Condition: CreateProdResources]]
  DevAndStaging:
    !And [Condition: CreateStagingResources, !Not [Condition: CreateProdResources]]
  DevAndProd:
    !And [!Not [Condition: CreateStagingResources], Condition: CreateProdResources]
  DevStagingProd:
    !And [Condition: CreateStagingResources, Condition: CreateProdResources]


#-------------------------------------------------------------------------
Resources:

  Dev:
    Condition: CreateDevResources
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Join ["", ["https://s3.amazonaws.com/",
                    {"Fn::ImportValue": {"Fn::Sub": "${OceanStack}-TemplateBucketName"}},
                    "/services/", !Ref ServiceName, "-env.yaml"]]
      Parameters:
        ServiceName: "auth"
        OceanEnv: "dev"
        OceanStack: !Ref OceanStack
        NumberOfAZs: !Ref NumberOfAZs
        PublicDomainName: !Ref DevPublicDomainName
        SizeOfEnvironment: !Ref DevSizeOfEnvironment
        TypeOfDeployment: !Ref DevTypeOfDeployment
        ALBListener: !Ref DevALBListener
        PrivateSubnetA: !Ref DevPrivateSubnetA
        PrivateSubnetB: !Ref DevPrivateSubnetB
        PrivateSubnetC: !Ref DevPrivateSubnetC
        MicroserviceSG: !Ref DevMicroserviceSG


  Staging:
    Condition: CreateStagingResources
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Join ["", ["https://s3.amazonaws.com/",
                    {"Fn::ImportValue": {"Fn::Sub": "${OceanStack}-TemplateBucketName"}},
                    "/services/", !Ref ServiceName, "-env.yaml"]]
      Parameters:
        ServiceName: "auth"
        OceanEnv: "staging"
        OceanStack: !Ref OceanStack
        NumberOfAZs: !Ref NumberOfAZs
        PublicDomainName: !Ref StagingPublicDomainName
        SizeOfEnvironment: !Ref StagingSizeOfEnvironment
        TypeOfDeployment: !Ref StagingTypeOfDeployment
        ALBListener: !Ref StagingALBListener
        PrivateSubnetA: !Ref StagingPrivateSubnetA
        PrivateSubnetB: !Ref StagingPrivateSubnetB
        PrivateSubnetC: !Ref StagingPrivateSubnetC
        MicroserviceSG: !Ref StagingMicroserviceSG


  Prod:
    Condition: CreateProdResources
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        !Join ["", ["https://s3.amazonaws.com/",
                    {"Fn::ImportValue": {"Fn::Sub": "${OceanStack}-TemplateBucketName"}},
                    "/services/", !Ref ServiceName, "-env.yaml"]]
      Parameters:
        ServiceName: "auth"
        OceanEnv: "prod"
        OceanStack: !Ref OceanStack
        NumberOfAZs: !Ref NumberOfAZs
        PublicDomainName: !Ref ProdPublicDomainName
        SizeOfEnvironment: !Ref ProdSizeOfEnvironment
        TypeOfDeployment: !Ref ProdTypeOfDeployment
        ALBListener: !Ref ProdALBListener
        PrivateSubnetA: !Ref ProdPrivateSubnetA
        PrivateSubnetB: !Ref ProdPrivateSubnetB
        PrivateSubnetC: !Ref ProdPrivateSubnetC
        MicroserviceSG: !Ref ProdMicroserviceSG


  ############################################################
  #
  # CodeDeploy Application and Deployment Groups
  #
  ############################################################

  CodeDeployApplication:
    Type: "AWS::CodeDeploy::Application"


  DevDeploymentGroup:
    Condition: CreateDevResources
    Type: "AWS::CodeDeploy::DeploymentGroup"
    Properties:
      ApplicationName: !Ref CodeDeployApplication
      AutoScalingGroups:
        - !GetAtt [Dev, Outputs.ASG]
      ServiceRoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"

  StagingDeploymentGroup:
    Condition: CreateStagingResources
    Type: "AWS::CodeDeploy::DeploymentGroup"
    Properties:
      ApplicationName: !Ref CodeDeployApplication
      AutoScalingGroups:
        - !GetAtt [Staging, Outputs.ASG]
      ServiceRoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"

  ProdDeploymentGroup:
    Condition: CreateProdResources
    Type: "AWS::CodeDeploy::DeploymentGroup"
    Properties:
      ApplicationName: !Ref CodeDeployApplication
      AutoScalingGroups:
        - !GetAtt [Prod, Outputs.ASG]
      ServiceRoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"


  ############################################################
  #
  # CodeBuild
  #
  ############################################################

  CodeBuildProject:
    Type: "AWS::CodeBuild::Project"
    Properties:
      Name: !Sub "${OceanStack}-${ServiceName}"
      Description: Testing the Rails app
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        EnvironmentVariables: []
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/ruby:2.3.1"
      ServiceRole:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"
      TimeoutInMinutes: 10
      Source:
        Type: CODEPIPELINE
      Artifacts:
        Type: CODEPIPELINE


  ############################################################
  #
  # Pipeline (four possibilities)
  #
  #   OnlyDev:          src, test, deploy to Dev.
  #   DevAndStaging:    src, test, deploy to Dev + Staging.
  #   DevAndProd:       src, test, deploy to Dev + Prod.
  #   DevStagingProd:   src, test, deploy to Dev, pause,
  #                     deploy to Staging, pause, deploy
  #                     to Prod.
  #
  ############################################################

  PipelineDev:
    Condition: OnlyDev
    Type: "AWS::CodePipeline::Pipeline"
    Properties:
      Name: !Sub "${OceanStack}-${ServiceName}"
      ArtifactStore:
        Type: S3
        Location:
          Fn::ImportValue:
            !Sub "${OceanStack}-Bucket"
      RoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"
      Stages:
        - Name: "Source"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                RepositoryName: !Ref ServiceName
                BranchName: "master"
              OutputArtifacts:
                - Name: "rails-app"
        - Name: "Test"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Test
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              Configuration:
                ProjectName: !Ref CodeBuildProject
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Dev"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref DevDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"


  PipelineDevStaging:
    Condition: DevAndStaging
    Type: "AWS::CodePipeline::Pipeline"
    Properties:
      Name: !Sub "${OceanStack}-${ServiceName}"
      ArtifactStore:
        Type: S3
        Location:
          Fn::ImportValue:
            !Sub "${OceanStack}-Bucket"
      RoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"
      Stages:
        - Name: "Source"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                RepositoryName: !Ref ServiceName
                BranchName: "master"
              OutputArtifacts:
                - Name: "rails-app"
        - Name: "Test"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Test
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              Configuration:
                ProjectName: !Ref CodeBuildProject
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Dev"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref DevDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Staging"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref StagingDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"


  PipelineDevProd:
    Condition: DevAndProd
    Type: "AWS::CodePipeline::Pipeline"
    Properties:
      Name: !Sub "${OceanStack}-${ServiceName}"
      ArtifactStore:
        Type: S3
        Location:
          Fn::ImportValue:
            !Sub "${OceanStack}-Bucket"
      RoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"
      Stages:
        - Name: "Source"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                RepositoryName: !Ref ServiceName
                BranchName: "master"
              OutputArtifacts:
                - Name: "rails-app"
        - Name: "Test"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Test
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              Configuration:
                ProjectName: !Ref CodeBuildProject
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Dev"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref DevDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Prod"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref ProdDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"


  PipelineDevStagingProd:
    Condition: DevStagingProd
    Type: "AWS::CodePipeline::Pipeline"
    Properties:
      Name: !Sub "${OceanStack}-${ServiceName}"
      ArtifactStore:
        Type: S3
        Location:
          Fn::ImportValue:
            !Sub "${OceanStack}-Bucket"
      RoleArn:
        Fn::ImportValue:
          !Sub "${OceanStack}-CodePipelineRoleARN"
      Stages:
        - Name: "Source"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                RepositoryName: !Ref ServiceName
                BranchName: "master"
              OutputArtifacts:
                - Name: "rails-app"
        - Name: "Test"
          Actions:
            -
              Name: !Ref ServiceName
              ActionTypeId:
                Category: Test
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              Configuration:
                ProjectName: !Ref CodeBuildProject
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Dev"
          Actions:
            -
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref DevDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Staging"
          Actions:
            -
              RunOrder: 1
              Name: "Approval1"
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Provider: Manual
                Version: '1'
            -
              RunOrder: 2
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref StagingDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"
        - Name: "Prod"
          Actions:
            -
              RunOrder: 1
              Name: "Approval2"
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Provider: Manual
                Version: '1'
            -
              RunOrder: 2
              Name: "AutoScalingGroup"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Provider: CodeDeploy
                Version: '1'
              Configuration:
                ApplicationName: !Ref CodeDeployApplication
                DeploymentGroupName: !Ref ProdDeploymentGroup
              InputArtifacts:
                - Name: "rails-app"


  ############################################################
  #
  # Service-wide SNS topic; for all environments
  #
  ############################################################

  ServiceWide:
    Type: "AWS::SNS::Topic"
    Properties:
      DisplayName: !Sub "${AWS::StackName} service-wide"
      Subscription: []


#-------------------------------------------------------------------------
Outputs:

  ServiceWideSNSTopic:
    Description: SNS Topic for service-wide notifications; for all environments
    Value: !Ref ServiceWide
    Export:
      Name:
        !Sub "${AWS::StackName}-ServiceWideSNSTopic"

  PipelineURL:
    Description: The URL of the pipeline on AWS
    Value: !Sub "https://${AWS::Region}.console.aws.amazon.com/codepipeline/home?region=${AWS::Region}#/view/${OceanStack}-${ServiceName}"
