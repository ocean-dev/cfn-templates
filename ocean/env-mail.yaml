#
# https://s3.amazonaws.com/ocean-templates/ocean/env-mail.yaml
#

AWSTemplateFormatVersion: "2010-09-09"

Description: "Ocean service Mail"


#-------------------------------------------------------------------------
Parameters:

  ServiceName:
    Description: The name of the service and of its DB, if any
    Type: String
    AllowedPattern: "^[a-z][-a-z0-9_]*$"

  ServicePassword:
    Description: The password to use when calling other services
    Type: String

  VpcStack:
    Description: The VPC stack for which this is done
    Type: String
    MinLength: 1
    MaxLength: 255
    AllowedPattern: "^[a-zA-Z][-a-zA-Z0-9]*$"
    Default: VPC

  VpcEnv:
    Description: The environment (master, staging, prod)
    Type: String

  InternalLbListener:
    Type: String

  DomainName:
    Type: String

  ECSCluster:
    Type: String

  ECSTaskExecutionRole:
    Type: String

  FargateContainerSecurityGroup:
    Description: A security group for the containers we will run in Fargate.
    Type: AWS::EC2::SecurityGroup::Id

  CloudwatchLogsGroup:
    Type: String

  EcsAutoScalingRoleArn:
    Type: String


  SubnetA:
    Type: AWS::EC2::Subnet::Id

  SubnetB:
    Type: AWS::EC2::Subnet::Id

  SubnetC:
    Type: AWS::EC2::Subnet::Id


  SetupTaskLambdaArn:
    Type: String


  VarnishCaches:
    Type: String


  SqlHost:
    Type: String

  SqlUsername:
    Type: String

  SqlPassword:
    Type: String

  SqlDbAdapter:
    Type: String

  SqlDbPort:
    Type: String


  TaskRole:
    Type: String


  EnvironmentEmail:
    Type: String


  MinClusterTasks:
    Type: String
    Default: "1"

  MaxClusterTasks:
    Type: String
    Default: "20"

  ContainerCpu:
    Type: String
    Default: "256"

  ContainerMemory:
    Type: String
    Default: "512"

  WebConcurrency:
    Type: String
    Default: "2"

  RailsMaxThreads:
    Type: String
    Default: "10"

  SqlConnectionPoolSize:
    Type: String
    Default: "20"


#-------------------------------------------------------------------------
Resources:

  ############################################################
  #
  # Target Group and Listener routing rules
  #
  ############################################################

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      VpcId:
        Fn::ImportValue:
          !Sub "${VpcStack}-VPC"
      Protocol: HTTP
      Port: 80
      TargetType: ip
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: '60'
      HealthCheckIntervalSeconds: 10
      HealthCheckPath: /alive
      HealthCheckPort: 80
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 2
      HealthyThresholdCount: 2

  ListenerRule170:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
        - Type: forward
          TargetGroupArn: !Ref TargetGroup
      Conditions:
        - Field: path-pattern
          Values:
            - "/v*/mails"
      ListenerArn: !Ref InternalLbListener
      Priority: 170


  ############################################################
  #
  # ECS task for setting up the service (rake ocean:setup_all)
  #
  ############################################################

  SetupAllTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Sub "${ServiceName}-setup_all"
      Cpu: !Ref ContainerCpu
      Memory: !Ref ContainerMemory
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      ExecutionRoleArn: !Ref ECSTaskExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: "rails"
          Cpu: !Ref ContainerCpu
          Memory: !Ref ContainerMemory
          Image: !Sub "peterbengtson/ocean_${ServiceName}_service"
          Command: ["rake ocean:setup_all"]
          PortMappings:
            - ContainerPort: 80
          LogConfiguration:
            LogDriver: "awslogs"
            Options:
              awslogs-group: !Sub "ocean-${VpcStack}-${VpcEnv}-logs"
              awslogs-region: !Ref "AWS::Region"
              awslogs-stream-prefix: !Sub "rake-tasks-${ServiceName}"
          Environment:
            - Name: OCEAN_ENV
              Value: !Ref VpcEnv
            - Name: RAILS_ENV
              Value: production
            - Name: APP_NAME
              Value: !Ref ServiceName
            - Name: API_USER
              Value: !Ref ServiceName
            - Name: API_PASSWORD
              Value: !Ref ServicePassword
            - Name: BASE_DOMAIN
              Value: !Ref DomainName
            - Name: OCEAN_API_HOST
              Value: !Sub "${VpcEnv}-api.${DomainName}"
            - Name: OCEAN_API_URL
              Value: !Sub "https://${VpcEnv}-api.${DomainName}"
            - Name: INTERNAL_OCEAN_API_URL
              Value: !Sub "http://${VpcEnv}-varnish.${DomainName}"
            - Name: VARNISH_CACHES
              Value: !Ref VarnishCaches
            - Name: API_VERSIONS
              Value: '{"_default":"v1"}'
            - Name: SQL_DB_ADAPTER
              Value: !Ref SqlDbAdapter
            - Name: SQL_CONNECTION_POOL_SIZE
              Value: !Ref SqlConnectionPoolSize
            - Name: SQL_HOST
              Value: !Ref SqlHost
            - Name: SQL_PORT
              Value: !Ref SqlDbPort
            - Name: SQL_DATABASE
              Value: !Ref ServiceName
            - Name: SQL_USERNAME
              Value: !Ref SqlUsername
            - Name: SQL_PASSWORD
              Value: !Ref SqlPassword
            - Name: AWS_REGION
              Value: !Ref "AWS::Region"
            - Name: RAILS_MAX_THREADS
              Value: !Ref RailsMaxThreads
            - Name: WEB_CONCURRENCY
              Value: !Ref WebConcurrency
            - Name: EMAIL_STEP_TIME
              Value: 30
            - Name: EMAIL_POISON_LIMIT
              Value: 10
            - Name: EMAIL_RETRY_BASE
              Value: 5.0
            - Name: EMAIL_RETRY_MULTIPLIER
              Value: 3.0
            - Name: EMAIL_RETRY_EXPONENT
              Value: 3.0
            - Name: ASYNC_JOB_VERSION
              Value: v1
            - Name: SMTP_ADDRESS
              Value: YourEmailSMTPHostHere
            - Name: SMTP_PORT
              Value: 25
            - Name: SMTP_USER_NAME
              Value: TheUsername
            - Name: SMTP_PASSWORD
              Value: ThePassword
            - Name: SMTP_AUTHENTICATION
              Value: login


  RunSetupEcsTask:
    Type: AWS::CloudFormation::CustomResource
    Properties:
      ServiceToken: !Ref SetupTaskLambdaArn
      TaskName: !Sub "${ServiceName}-setup_all"
      TaskArn: !Ref SetupAllTaskDefinition


  ############################################################
  #
  # ECS service and associated task
  #
  ############################################################

  ServiceTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    DependsOn: RunSetupEcsTask
    Properties:
      Family: !Sub "${ServiceName}_service"
      Cpu: !Ref ContainerCpu
      Memory: !Ref ContainerMemory
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      ExecutionRoleArn: !Ref ECSTaskExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: "rails"
          Cpu: !Ref ContainerCpu
          Memory: !Ref ContainerMemory
          Image: !Sub "peterbengtson/ocean_${ServiceName}_service"
          PortMappings:
            - ContainerPort: 80
          LogConfiguration:
            LogDriver: "awslogs"
            Options:
              awslogs-group: !Sub "ocean-${VpcStack}-${VpcEnv}-logs"
              awslogs-region: !Ref "AWS::Region"
              awslogs-stream-prefix: !Ref ServiceName
          Environment:
            - Name: OCEAN_ENV
              Value: !Ref VpcEnv
            - Name: RAILS_ENV
              Value: production
            - Name: APP_NAME
              Value: !Ref ServiceName
            - Name: API_USER
              Value: !Ref ServiceName
            - Name: API_PASSWORD
              Value: !Ref ServicePassword
            - Name: BASE_DOMAIN
              Value: !Ref DomainName
            - Name: OCEAN_API_HOST
              Value: !Sub "${VpcEnv}-api.${DomainName}"
            - Name: OCEAN_API_URL
              Value: !Sub "https://${VpcEnv}-api.${DomainName}"
            - Name: INTERNAL_OCEAN_API_URL
              Value: !Sub "http://${VpcEnv}-varnish.${DomainName}"
            - Name: VARNISH_CACHES
              Value: !Ref VarnishCaches
            - Name: API_VERSIONS
              Value: '{"_default":"v1"}'
            - Name: SQL_DB_ADAPTER
              Value: !Ref SqlDbAdapter
            - Name: SQL_CONNECTION_POOL_SIZE
              Value: !Ref SqlConnectionPoolSize
            - Name: SQL_HOST
              Value: !Ref SqlHost
            - Name: SQL_PORT
              Value: !Ref SqlDbPort
            - Name: SQL_DATABASE
              Value: !Ref ServiceName
            - Name: SQL_USERNAME
              Value: !Ref SqlUsername
            - Name: SQL_PASSWORD
              Value: !Ref SqlPassword
            - Name: AWS_REGION
              Value: !Ref "AWS::Region"
            - Name: RAILS_MAX_THREADS
              Value: !Ref RailsMaxThreads
            - Name: WEB_CONCURRENCY
              Value: !Ref WebConcurrency
            - Name: EMAIL_STEP_TIME
              Value: 30
            - Name: EMAIL_POISON_LIMIT
              Value: 10
            - Name: EMAIL_RETRY_BASE
              Value: 5.0
            - Name: EMAIL_RETRY_MULTIPLIER
              Value: 3.0
            - Name: EMAIL_RETRY_EXPONENT
              Value: 3.0
            - Name: ASYNC_JOB_VERSION
              Value: v1
            - Name: SMTP_ADDRESS
              Value: YourEmailSMTPHostHere
            - Name: SMTP_PORT
              Value: 25
            - Name: SMTP_USER_NAME
              Value: TheUsername
            - Name: SMTP_PASSWORD
              Value: ThePassword
            - Name: SMTP_AUTHENTICATION
              Value: login

  Service:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: !Ref ServiceName
      Cluster: !Ref ECSCluster
      LaunchType: FARGATE
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 75
      DesiredCount: 1
      NetworkConfiguration:
        AwsvpcConfiguration:
          SecurityGroups: [!Ref FargateContainerSecurityGroup]
          Subnets: [!Ref SubnetA, !Ref SubnetB, !Ref SubnetC]
      TaskDefinition: !Ref ServiceTaskDefinition
      LoadBalancers:
        - ContainerName: "rails"
          ContainerPort: 80
          TargetGroupArn: !Ref TargetGroup


  ############################################################
  #
  # Autoscaling for the above service
  #
  ############################################################

  AutoScalingTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    DependsOn: Service
    Properties:
      MaxCapacity: !Ref MaxClusterTasks
      MinCapacity: !Ref MinClusterTasks
      ResourceId: !Sub "service/${ECSCluster}/${ServiceName}"
      RoleARN: !Ref EcsAutoScalingRoleArn
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs

  AutoScalingPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Sub "${VpcStack}-${VpcEnv}-${ServiceName}-EcsScalingPolicy"
      PolicyType: StepScaling
      ScalingTargetId: !Ref AutoScalingTarget
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      StepScalingPolicyConfiguration:
        AdjustmentType: ChangeInCapacity
        Cooldown: 60
        MetricAggregationType: Average
        StepAdjustments:
        - MetricIntervalLowerBound: 0
          ScalingAdjustment: 2
        - MetricIntervalUpperBound: 0
          ScalingAdjustment: -1

  AutoScalingCPUAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: Containers CPU Utilization High
      MetricName: CPUUtilization
      Namespace: AWS/ECS
      Statistic: Average
      Period: '60'
      EvaluationPeriods: '3'
      Threshold: '80'
      AlarmActions:
        - !Ref AutoScalingPolicy
      Dimensions:
      - Name: ServiceName
        Value: !Ref ServiceName
      - Name: ClusterName
        Value: !Ref ECSCluster
      ComparisonOperator: GreaterThanOrEqualToThreshold
